import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';


import './index.css';
import App from './components/App';
import PhotoGrid from './components/PhotoGrid';
import Single from './components/Single';
import NotFound from './components/NotFound';

const Root = () => {
    return (
        <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={App} />
                    <Route path='/photo' component={PhotoGrid} />
                    <Route path='/view' component={Single} />
                    <Route component={NotFound} />
                </Switch>
        </BrowserRouter>
    )
}

render(<Root />, document.getElementById('root'));

